
/*   links for services  on click cards   */
$('#goTechService').click(function(){
	location.href = "tech-service.html";
});

$('#goEmerDispatch').click(function(){
	location.href = "emer-dispatch.html";
});

$('#goDispatch').click(function(){
	location.href = "dispatch.html";
});

$('#goModern').click(function(){
	location.href = "modern.html";
});
$('#goRepairs').click(function(){
	location.href = "repairs.html";
});


/*   Animations services on load   */
$(document).ready(function(){
	// if($(window).width() > 1199) {
		servicesLoadAnimate();
	// };
});


function servicesLoadAnimate() {
	$('header').addClass('animate');
	$('.services__section-1').addClass('animate');

	setTimeout(function(){
		$('.banner.main').addClass('animate');
	}, 1000);

	setTimeout(function(){
		$('.services__section-1-title').addClass('animate');
	}, 1300);

	setTimeout(function(){
		$('.sitephone').addClass('animate');
		$('.services__section-2').addClass('animate');
	}, 1600);
};
