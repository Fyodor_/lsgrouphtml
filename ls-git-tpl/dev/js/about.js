
/*   SLIDER   */
$(document).ready(function(){
	/*   slider section-4   */
	$('.about__section-3-slider').slick({
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		fade: true,
		pauseOnHover: false
	});
	$('.about__section-3-slider').find('.slick-dots').addClass('dots-about');

	/*   Animations page about   */
	// if($(window).width() > 1199) {
		aboutLoadAnimate();
		aboutScrollAnimate();
		animateSectionAbout();
	// };
});


// $(window).resize(function(){
// 	if($(window).width() > 1199) {
// 		aboutLoadAnimate();
// 		aboutScrollAnimate();
// 		animateSectionAbout();
// 	};
// });

function aboutLoadAnimate(){
	$('header').addClass('animate');
	$('.about__section-1').addClass('animate');

	setTimeout(function(){
		$('.banner.main').addClass('animate');
	}, 800);

	setTimeout(function(){
		$('.about__section-1-title').addClass('animate');
	}, 1200);

	setTimeout(function(){
		$('.sitephone').addClass('animate');
		$('.about__section-1-info').addClass('animate');
	}, 1500);
};

function aboutScrollAnimate() {
	$(window).scroll(function(){
		animateSectionAbout();
	});
};

function animateSectionAbout() {
	var section2ImgDist = 200;
	var section2CntDist = 350;	
	var aboutSection3Dist = $('.about__section-2').offset().top;
	var aboutSection4Dist = $('.about__section-3').offset().top;

	if ($(window).scrollTop() > section2ImgDist){
		$('.about__section-2-img').addClass('animate');
	};

	if ($(window).scrollTop() > section2CntDist){
		$('.about__section-2-cnt').addClass('animate');
	};

	if ($(window).scrollTop() > aboutSection3Dist + 100){
		$('.about__section-3-slider').addClass('animate');
	};

	if ($(window).scrollTop() > aboutSection3Dist + 300){
		$('.about__section-3-descr').addClass('animate');
	};

	if ($(window).scrollTop() > aboutSection4Dist){
		$('.about__section-4-img').addClass('animate');
	};

	if ($(window).scrollTop() > aboutSection4Dist + 150){
		$('.about__section-4-cnt').addClass('animate');
	};
};