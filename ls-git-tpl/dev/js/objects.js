
$(document).ready(function(){
	objectsAnimate();

	if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i)){
		$('.object__map-cnt').addClass('for__msie');
		$('.objects__content .container').css('margin-top', '0');
		changePos();
	};	
});


function changePos() {
	$(window).scroll(function(){
		if($(window).scrollTop() > $('.objects__content').height() - $(window).height()) {
			$('.object__map-cnt').addClass('sticky');
		} else {
			$('.object__map-cnt').removeClass('sticky');
		};
	});
};

// $(window).resize(function(){
// 	if($(window).width() > 1199) {
// 		objectsAnimate();	
// 	};
// });

/*   animations contacts  */
function objectsAnimate() {
	$('header').addClass('animate');

	setTimeout(function(){
		$('.objects__content .container ').addClass('animate');
	}, 900);

	setTimeout(function(){
		initMap();         /*   initialization map   */
	}, 200);

	setTimeout(function(){
		$('#objets-map').addClass('animate');
	}, 1500);
};


/*   Click to load images of objects   */
$('.media-show--767').on('click', function(){
	$('.img-load img').attr("src","dev/svg/load_dark.svg");
	$('.img-load').addClass('active');
	setTimeout(function(){
		$('.col-6.media-hide--767').addClass('show');
		$('.col-12.media-show--767').css('display', 'none');
	}, 1000);
});


/*   Objects coordinates    */
var points = [
[55.745313, 37.662197],[55.832658, 37.618254],[55.725444, 37.754907],
[55.796431, 37.487109],[55.753442, 37.577221],[55.741626, 37.537939],
[55.774281, 37.651772],[55.834090, 37.636784],[55.714698, 37.504717],
[55.792077, 37.786578],[55.787916, 37.680297],[55.798200, 37.520212],
[55.773798, 37.588094],[55.751480, 37.584406],[55.823422, 37.619675],
[55.671563, 37.630286],[55.749776, 37.577206],[55.832509, 37.485475],
[55.694956, 37.530879],[55.948425, 37.418622],[55.741830, 37.417234],
[55.809205, 37.464775],[55.693372, 37.534297],[55.854855, 37.477379],
[55.846400, 37.660375],[55.787861, 37.678004],[55.788590, 37.678712],
[55.784818, 37.697845],[55.679806, 37.468493],[55.702482, 37.494238],
[55.725593, 37.570942],[55.743489, 37.860394],[55.730003, 37.574263],
[55.730456, 37.740077],[55.710528, 37.656920],
/*not indeficated elements*/
[55.865684, 37.593629],[55.880130, 37.597062],[55.870885, 37.562386],
[55.862794, 37.533547],[55.802437, 37.714711],[55.888848, 37.588958],
[55.843895, 37.440386],[55.770222, 37.737531],[55.820989, 37.511759],
[55.801891, 37.399115],[55.733061, 37.420964],[55.695043, 37.547203],
[55.707628, 37.738988],[55.693401, 37.519527],[55.884219, 37.604010],
[55.847438, 37.365128],[55.800527, 37.799193],[55.676157, 37.496222],
[55.646030, 37.611293],[55.736763, 37.674271],[55.743870, 37.521329],
[55.784570, 37.721368],[55.599337, 37.608882],[55.607677, 37.542964],
[55.649671, 37.543049],[55.706736, 37.474984],[55.760535, 37.566091],
[55.765364, 37.648317],[55.679806, 37.468493],[55.762756, 37.638189],
[55.774801, 37.515691],[55.744374, 37.763451],[55.645109, 37.497957],
[55.636114, 37.797070],[55.890514, 37.654890],[55.659415, 37.752096],
[55.668205, 37.743832],[55.666163, 37.782083],[55.674996, 37.755480],
[55.649681, 37.719948],[55.621171, 37.704895],[55.619751, 37.742156],
[55.609659, 37.718860],[55.627408, 37.655894],[55.704552, 37.765751],

[55.751642, 37.598758],[55.741359, 37.625657],[55.765041, 37.603652],
[55.758820, 37.622444],[55.772277, 37.619896],[55.740675, 37.655535]

];


var card1 = {lat: 55.745313, lng: 37.662197};
var centerMap = {lat: 55.773798, lng: 37.588094};

var svgIcon1 = {
	path: google.maps.SymbolPath.CIRCLE,
	strokeColor: '#FFF4F2',
	strokeOpacity: 1,
	strokeWeight: 1,
	fillColor: '#F6905A',
	fillOpacity: 1,
	rotation: 0,
	scale: 4
};
var svgIcon2 = {
	path: 'M6,0A6,6,0,0,0,0,6c0,3.85,4.32,8.66,5,9.4.28.31.52.6,1,.6s.72-.3,1-.6c.68-.73,5-5.54,5-9.4A6,6,0,0,0,6,0ZM6,10a4,4,0,1,1,4-4A4,4,0,0,1,6,10ZM6,4A2,2,0,1,0,8,6,2,2,0,0,0,6,4Z',
	strokeColor: '#FFF4F2',
	strokeOpacity: 1,
	strokeWeight: 1,
	fillColor: '#F6905A',
	fillOpacity: 1,
	rotation: 0,
	scale: 2,
	anchor: new google.maps.Point(6, 14),
};

var svgIcon = svgIcon1;

var markers = [];


$('.col-6').each(function () {
	$(this).attr('id');
});

$('.col-6').hover(function(){
	markers[this.id].setIcon(svgIcon2);
},function(){
	markers[this.id].setIcon(svgIcon1);
});


function initMap(){
	if($(window).width() > 1199 ) {
		var map = new google.maps.Map(document.getElementById('objets-map'), {
			center: centerMap,
			zoom: 10,
			scrollwheel: false,
			mapTypeControl: false,
			zoomControl: false,
			streetViewControl: false
		});

		var objCount = points.length-1;
		for(var i=0; i<=objCount; i++){
			var latLng = new google.maps.LatLng(points[i][0], points[i][1]);
			markers.push(new google.maps.Marker({
				position: latLng,
			//position: card1,
			map: map,
			icon: svgIcon,
		}));
		};

		google.maps.event.addDomListener(window, 'resize', function() {
			var center = map.getCenter()
			google.maps.event.trigger(map, "resize")
			map.setCenter(center)
		});
	};
};
