$(document).ready(function(){
	fotoramaOnLoad();
});


/*   Changing fotorama on page   */
var $fotoramaDiv = $('.fotorama').fotorama();
var fotorama = $fotoramaDiv.data('fotorama');
var lastSlide = fotorama.size;
var activeSlide = fotorama.activeIndex;


/*window options*/
function fotoramaOnLoad(){
	if($(window).width() <= 767) {
		fotorama.setOptions({
			nav: 'dots'
		});
	};
	if($(window).width() <= 1199) {
		fotorama.setOptions({
			width: '75%',
			height: '340'
		});
	};
};

/*changing options to resize*/
$(window).resize(function(){
	if($(window).width() < 768) {
		fotorama.setOptions({
			nav: 'dots'
		});
	} else {
		fotorama.setOptions({
			nav: 'thumbs'
		});
	};

	if($(window).width() < 1200) {
		fotorama.setOptions({
			width: '75%',
			height: '340'
		});
	} else {
		fotorama.setOptions({
			width: '60%'
		});
	};
});

/*changing opacity buttons*/
if( activeSlide == 0 ) {
	$('.prev-slide').css('opacity', '0.2');
};

$('.fotorama').on('fotorama:show', function () {
	activeSlide = fotorama.activeIndex;

	if( activeSlide == 0 ) {
		$('.prev-slide').css('opacity', '0.2');
	} else {
		$('.prev-slide').css('opacity', '1');
	};

	if( activeSlide == lastSlide - 1 ) {
		$('.next-slide').css('opacity', '0.2');
	} else {
		$('.next-slide').css('opacity', '1');
	};
});

/*change slide on prev/next click buttons*/
$('.prev-slide').on('click', function(){
	fotorama.show('<');
	activeSlide = fotorama.activeIndex;
});

$('.next-slide').on('click', function(){
	fotorama.show('>');
	activeSlide = fotorama.activeIndex;
});


/*   Changing fotorama on fullsize window   */
var $fotoramaInner = $('.foto-fullscreen').fotorama();
var fotoramaFull = $fotoramaInner.data('fotorama');
var lastSlideFull = fotoramaFull.size;
var activeSlideFull = fotoramaFull.activeIndex;

$('.fotorama__stage').on('click', function() {	
	$('.zoom-window-wrap').css('z-index', '10').css('opacity', '1');
	setTimeout(function(){
		$('.zoom-modal-window').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
	}, 300);
	scrollDistance = 0;
	fotoramaFull.show(activeSlide);

	if($(window).width() <= 767) {
		fotoramaFull.setOptions({
			height: '400'
		});
	};

	if($(window).width() <= 575) {
		fotoramaFull.setOptions({
			height: '300',
			width: '300',
			nav: 'dots'
		});
	};
});

/*close fullsize window*/
$('.close-fullsize').on('click', function() {
	$('.zoom-modal-window').css('transform', 'translate(-75%, -50%)');
	$('.zoom-modal-window').css('opacity', '0')
	setTimeout(function(){
		$('.zoom-window-wrap').css('opacity', '0');	
	}, 800);
	setTimeout(function(){
		$('.zoom-window-wrap').css('z-index', '-1');
	}, 1200);
	scrollDistance = 100;
});

$('.close-fullsize-mobile').on('click', function() {
	$('.zoom-modal-window').css('transform', 'translate(-75%, -50%)');
	$('.zoom-modal-window').css('opacity', '0')
	setTimeout(function(){
		$('.zoom-window-wrap').css('opacity', '0');	
	}, 800);
	setTimeout(function(){
		$('.zoom-window-wrap').css('z-index', '-1');
	}, 1200);
	scrollDistance = 100;
});


/*changing opacity buttons*/
if( activeSlideFull== 0 ) {
	$('.prev-slide-fullsize').css('opacity', '0.2');
};

$('.foto-fullscreen').on('fotorama:show', function () {
	activeSlideFull = fotoramaFull.activeIndex;


	if( activeSlideFull == 0 ) {
		$('.prev-slide-fullsize').css('opacity', '0.2');
	} else {
		$('.prev-slide-fullsize').css('opacity', '1');
	};

	if( activeSlideFull == lastSlideFull - 1 ) {
		$('.next-slide-fullsize').css('opacity', '0.2');
	} else {
		$('.next-slide-fullsize').css('opacity', '1');
	};
});

/*change slide  on prev/next click buttons for fotorama full size*/
$('.prev-slide-fullsize').on('click', function(){
	fotoramaFull.show('<');
	activeSlideFull = fotoramaFull.activeIndex;
});

$('.next-slide-fullsize').on('click', function(){
	fotoramaFull.show('>');
	activeSlideFull = fotoramaFull.activeIndex;
});