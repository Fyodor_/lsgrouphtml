
$(document).ready(function() {
	// if($(window).width() > 1199) {
		contactsAnimate();
	// };
});


// $(window).resize(function(){
// 	if($(window).width() > 1199) {
// 		contactsAnimate();
// 	};
// });

/*   Animations contacts  */
function contactsAnimate() {

	setTimeout(function(){
		$('header').addClass('animate');
	}, 500);

	setTimeout(function(){
		$('.contacts__section-1').addClass('animate');
	}, 1500);

	setTimeout(function(){
		$('.sitephone').addClass('animate');	
	}, 1500);

	setTimeout(function(){
		initMap();	/*   initializing map after reload body   */	
	}, 2500);

	setTimeout(function(){
		$('#map').addClass('animate');
	}, 3000);

	setTimeout(function(){
		maskInput();
	}, 3000);
};


/*   initialization map   */
function initMap() {

	var centerMap;
	if($(window).width() < 768) {
		centerMap = {lat: 55.666279, lng: 37.618067}; 
	} else {
		centerMap = {lat:55.666222, lng: 37.619683}; 
	};
	
	var map = new google.maps.Map(document.getElementById('map'), {
		center: centerMap,
		zoom: 17,
		scrollwheel: false,
		mapTypeControl: false,
	});
	var marker = new google.maps.Marker({
		position: {lat:55.666222, lng: 37.619683},
		map: map
			// icon: {
			// 	url: "../svg/",
			// 	scaledSize: new google.maps.Size(64, 64)
			// }
		});

	google.maps.event.addDomListener(window, 'resize', function() {
		var center = map.getCenter()
		google.maps.event.trigger(map, "resize")
		map.setCenter(center)
	});
}


/*   autoresize for textarea   */
$(function() {
	$('.contacts__input-decor textarea').autoResize({
		paddingBottom: 0,
		maxHeight: 80,
		animateDuration: 100,
		handResizing: false,
		paddingBottom: 5,
	});
});


/*   INPUT-MASK section-9  */
function maskInput(){
	$('.form-phone').mask("+7(999)999-99-99");
};


/*   Validation forms   */
$('.form-name').focusout(function(){
	var valueName = $(this).val(),
	regularName =  /^[А-Я][а-яА-ЯёЁ]{2,20}/;
	if (!regularName.test(valueName)) {
		$(this).css('border-color', '#ff7373');
	} else {
		$(this).css('border-color', '#7b96cd');
	};
});

$('.form-phone').focusout(function(){
	var valuePhone = $(this).val(),
	regularPhone = /^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/;
	if (!regularPhone.test(valuePhone)) {
		$(this).css('border-color', '#ff7373');
	} else {
		$(this).css('border-color', '#7b96cd');
	};
});

$('.form-mail').focusout(function(){
	var valuePhone = $(this).val(),
	regularPhone = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/;
	if (!regularPhone.test(valuePhone)) {
		$(this).css('border-color', '#ff7373');
	} else {
		$(this).css('border-color', '#7b96cd');
	};
});

$('.form-mail').focusout(function(){
	var valuePhone = $(this).val(),
	regularPhone = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/;
	if (!regularPhone.test(valuePhone)) {
		$(this).css('border-color', '#ff7373');
	} else {
		$(this).css('border-color', '#7b96cd');
	};
});

$('.form-text').focusout(function(){
	if ($(this).val().length < 5) {
		$(this).css('border-color', '#ff7373');
	} else {
		$(this).css('border-color', '#7b96cd');
	};
});


/*   ajax reguest for mail user message   */
$(function(){
    $('#guest_form').submit(function(event) { //устанавливаем событие отправки для формы с id=form
    	event.preventDefault();
            var form_data = $(this).serialize(); //собераем все данные из формы
            $.ajax({
            type: "POST", //Метод отправки
            url: "call.php", //путь до php фаила отправителя
            data: form_data,
            success: function() {
                   //код в этом блоке выполняется при успешной отправке сообщения
                   alert("Ваше сообщение отпрвлено!");
                   $('#guest_form input').val('').prop('checked', false);
                   $('#guest_form textarea').val('');
               },
               error: function() {
               	alert("При отправке возникла ошибка");
               }
           });

        });
}); 


/*   ajax reguest for mail message sitephone form   */

var namePage = $('title').text(); /*window.location.pathname.toString()*/
$('#page_name').val(namePage);

$(function(){
    $("#reguest_form").submit(function(event) { //устанавливаем событие отправки для формы с id=form
    	event.preventDefault();
            var form_data = $(this).serialize(); //собераем все данные из формы
            $.ajax({
            type: "POST", //Метод отправки
            url: "call.php", //путь до php фаила отправителя
            data: form_data,
            success: function() {
                   //код в этом блоке выполняется при успешной отправке сообщения
                   alert("Ваше сообщение отпрвлено!");
                   $('#reguest_form input').val('').not(':hidden').prop('checked', false);

                   $('.modal-window').css('transform', 'translate(-75%, -50%)');
                   $('.modal-window').css('opacity', '0')
                   setTimeout(function(){
                   	$('.modal-window-wrap').css('opacity', '0');	
                   }, 800);
                   setTimeout(function(){
                   	$('.modal-window-wrap').css('z-index', '-1');
                   }, 1200);
                   scrollDistance = 100;
               },
               error: function() {
               	alert("При отправке возникла ошибка");
               }
           });

        });
});    