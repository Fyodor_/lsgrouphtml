/*   SHOW/HIDE BACKGROUND MENU   */
$(document).ready(function(){

	if($(window).width() <= 1023) {
		$('header').addClass('open');
		$('header').addClass('mobile');
		$('.section-1__nav ul').addClass('container');
	};

	$('header').addClass('bg-show');
	$('#h-logo').attr("src","dev/svg/LS_logo_11-01.svg");
});


$(window).resize(function(){
	if($(window).width() <= 1023) {
		$('header').addClass('open');
		$('header').addClass('mobile');
		$('.section-1__nav ul').addClass('container');
	} else {
		$('header').removeClass('mobile');
		$('header').addClass('bg-show');
		// $('header').removeClass('open');
		$('.section-1__nav ul').removeClass('container');
	};
});


// /*   Changing color theme navigation */
// $(window).scroll(function(){
// 	var distanse_menu = $('body').offset().top;
// 	if($(window).width() > 1023) {
// 		if ($(window).scrollTop() > distanse_menu){
// 			/*   changing color nav links and logo in header after show bg menu   */
// 			$('#h-logo').attr("src","dev/svg/LS_logo_11-01.svg");
// 			$('header').addClass('bg-show');
// 		} else {
// 			/*   changing color nav links and logo in header after show/hide bg menu   */
// 			$('#h-logo').attr("src","dev/svg/LS_logo_1.svg");
// 			$('header').removeClass('bg-show');
// 		};
// 	};	
// });


/*   OPEN/CLOSE header-descr for scroll with close-click   */
var closeDescr = false;
function closeHeaderDescr() {
	$('header').removeClass('open');
	closeDescr = true;
	if($('.category-items__select').css('position') === 'fixed'){
        $('.category-items__select').css('top', '70px');
	}
}

$(window).scroll(function(){
	if($(window).width() > 1023) {
		if($(window).scrollTop() > 1 && closeDescr == false){
			$('header').addClass('open');

		} else {
			$('header').removeClass('open');
		};
	};
});


/*   Add hover effect on link (arrow up/down)   */
if($(window).width() > 1023) {	
	$('.section-1__nav li.menu-list').hover(function(){
		$(this).find('span.ion-ios-arrow-down').removeClass('ion-ios-arrow-down').addClass('ion-ios-arrow-up');
	}, function(){
		$(this).find('span.ion-ios-arrow-up').removeClass('ion-ios-arrow-up').addClass('ion-ios-arrow-down');
	});
};


/*   Open/Close mobile menu   */
var navMediaOpen = 0;
var subMenuOpen = 0;
$('button.media-768').on('click', function(){
	if(navMediaOpen == 0) {
		$('.but-menu-icon').addClass('active');
		$('.nav-wrap').addClass('open');
		$(this).removeClass('ion-navicon').addClass('ion-ios-close-empty');
		navMediaOpen = 1;
	} else {
		$('.but-menu-icon').removeClass('active');
		$('.nav-wrap').removeClass('open');
		$(this).removeClass('ion-ios-close-empty').addClass('ion-navicon');
		navMediaOpen = 0;

		if(subMenuOpen == 1) {

		$('ul.sub-menu.services').css('z-index', '-1');
		$('ul.sub-menu.services').removeClass('open');
		$('li.menu-list.sub-menu.services').removeClass('open');
		$('.arrow-button-services').removeClass('active');

		$('ul.sub-menu.catalog').css('z-index', '-1');
		$('ul.sub-menu.catalog').removeClass('open');
		$('li.menu-list.sub-menu.catalog').removeClass('open');
		$('.arrow-button-catalog').removeClass('active');

		subMenuOpen = 0;
		
		}; 
	};
});


// /*   Open/Close mobile sub-menu   */
// $('.arrow-button').on('click', function(){
// 	if(subMenuOpen == 0) {
// 		$('li.menu-list.sub-menu').addClass('open');
// 		$(this).addClass('active');
// 		$('ul.sub-menu').addClass('open');
// 		setTimeout(function(){
// 		$('ul.sub-menu').css('z-index', 'auto');
// 	}, 500);	
// 		subMenuOpen = 1;
// 	} else {
// 		$('ul.sub-menu').css('z-index', '-1');
// 		$('ul.sub-menu').removeClass('open');
// 		$('li.menu-list.sub-menu').removeClass('open');
// 		$(this).removeClass('active');
// 		subMenuOpen = 0;
// 	}
// });


/*   Open/Close mobile sub-menu-services   */
$('.arrow-button-services').on('click', function(){
	if(subMenuOpen == 0) {
		$('li.menu-list.sub-menu.services').addClass('open');
		$(this).addClass('active');
		$('ul.sub-menu.services').addClass('open');
		setTimeout(function(){
		$('ul.sub-menu.services').css('z-index', 'auto');
	}, 500);	
		subMenuOpen = 1;
	} else {
		$('ul.sub-menu.services').css('z-index', '-1');
		$('ul.sub-menu.services').removeClass('open');
		$('li.menu-list.sub-menu.services').removeClass('open');
		$(this).removeClass('active');
		subMenuOpen = 0;
	}
});

/*   Open/Close mobile sub-menu-catalog   */
$('.arrow-button-catalog').on('click', function(){
	if(subMenuOpen == 0) {
		$('li.menu-list.sub-menu.catalog').addClass('open');
		$(this).addClass('active');
		$('ul.sub-menu.catalog').addClass('open');
		setTimeout(function(){
		$('ul.sub-menu.catalog').css('z-index', 'auto');
	}, 500);	
		subMenuOpen = 1;
	} else {
		$('ul.sub-menu.catalog').css('z-index', '-1');
		$('ul.sub-menu.catalog').removeClass('open');
		$('li.menu-list.sub-menu.catalog').removeClass('open');
		$(this).removeClass('active');
		subMenuOpen = 0;
	}
});