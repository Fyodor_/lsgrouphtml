/*   animation services for page that contains main-service__section-3  */
$(window).scroll(function(){
	animateSection3();
});

// $(window).resize(function(){
// 	if($(window).width() > 1199) {
// 		animateSection3();
// 	};
// });

function animateSection3() {
	var mainServiceSection3 = $('.main-service__section-3').offset().top;
	if ($(window).scrollTop() > mainServiceSection3 - 600){
		$('.main-service__section-3 .container').addClass('animate');
	};
};