
/*   Select structure   */

// function selectStructureInit(nameParent){
//   var markerClick = true;

//   $(nameParent).find('.select-calc span').click(function(){
//     if(markerClick == true){
//       $(nameParent).find('.select-calc ul').addClass('active');
//       $(nameParent).find('.select-calc').css('background-image', 'url(dev/svg/arrow_b_up.svg)');
//       markerClick = false;
//     } else {
//       $(nameParent).find('.select-calc ul').removeClass('active');
//       $(nameParent).find('.select-calc').css('background-image', 'url(dev/svg/arrow_b_down.svg)');
//       markerClick = true;
//     };
//   });

//   $(nameParent).find('.select-calc ul li').click(function(){
//     $(nameParent).find('.select-calc ul').removeClass('active');
//     $(nameParent).find('.select-calc').css('background-image', 'url(dev/svg/arrow_b_down.svg)');

//     $(nameParent).find('.select-calc span').html($(this).html());
//     $(nameParent).find('.type-structure').val($(this).html());
//     $(nameParent).find('.select-calc span').css('color', '#000').css('font-weight', 'bold');
//     markerClick = true;
//   });

//   $(document).click(function(event) {
//     if ($(nameParent).find(event.target).closest('.select-calc').length) return;
//     $(nameParent).find('.select-calc ul').removeClass('active');
//     $(nameParent).find('.select-calc').css('background-image', 'url(dev/svg/arrow_b_down.svg)');
//     event.stopPropagation();
//     markerClick = true;
//   });
// };

/*__________Jquery plugins__________*/

/*   Rage slider-stops   */
function sliderStopsInit(nameParent) {
  $(nameParent).find('.rage-slider-stop').ionRangeSlider({
    min: 2,
    max: 30,
    grid: true,
    grid_snap: true,
  });
};

/*   Rage slider-weight   */
function sliderWeightInit(nameParent) {
  $(nameParent).find('.slider-pos-1').css('font-weight', 'bold');/*begining style .slider-weight-value*/

  $(nameParent).find('.rage-slider-weight').ionRangeSlider({
    min: 1,
    max: 3,
    grid: true,
    grid_snap: true,
    /*   Styling .slider-weight-value items on change slider   */
    onChange: function(){
      var posSliderWeight = parseInt($(nameParent).find('.calc__form-slider-weight .irs-single').html());
      switch (posSliderWeight) {
        case 1: 
        $(nameParent).find('.slider-pos-1').css('font-weight', 'bold');
        $(nameParent).find('.slider-pos-2').css('font-weight', '400');
        $(nameParent).find('.slider-pos-3').css('font-weight', '400');
        break;
        case 2: 
        $(nameParent).find('.slider-pos-1').css('font-weight', '400'); 
        $(nameParent).find('.slider-pos-2').css('font-weight', 'bold'); 
        $(nameParent).find('.slider-pos-3').css('font-weight', '400'); 
        break;
        case 3: 
        $(nameParent).find('.slider-pos-1').css('font-weight', '400'); 
        $(nameParent).find('.slider-pos-2').css('font-weight', '400'); 
        $(nameParent).find('.slider-pos-3').css('font-weight', 'bold');
        break;
      }
    }
  });
};


/*   Tabs to select country   */
function selectCountyInit(nameParent){

  $(nameParent).find('.form-tab1').addClass('check');
  $(nameParent).find('.country').val($(nameParent).find('.form-tab1.check').html());

  $(nameParent).find('.form-tab1').on('click', function(){
    $(nameParent).find('.form-tab1').addClass('check');
    $(nameParent).find('.form-tab2').removeClass('check');
    $(nameParent).find('.country').val($(this).html());        
  });

  $(nameParent).find('.form-tab2').on('click', function(){
    $(nameParent).find('.form-tab2').addClass('check');
    $(nameParent).find('.form-tab1').removeClass('check');
    $(nameParent).find('.country').val($(this).html());        
  });
};


/*   Buttons for change quantity of lifts   */
function add_Remove_Lift(nameParent){
  var quantity = parseInt($(nameParent).find('.quantity-lifts').val());

  if( quantity < 1 ) {
   $(nameParent).find('.remove-lift').css('background', '#ebebeb').css('border-color', '#cccccc');
 }; 

 $(nameParent).find('.remove-lift').on('click', function(e){
  e.preventDefault(); 

  if( quantity != 0 ) {
    quantity--;
    $(nameParent).find('.quantity-lifts').val(quantity);
  };

  if( quantity < 1 ) {
   $(nameParent).find('.remove-lift').css('background', '#ebebeb').css('border-color', '#cccccc');
   $(nameParent).find('.remove-lift').hover(function(){
    $(this).css('background', '#ebebeb').css('border-color', '#cccccc')
  },
  function(){
    $(this).css('background', '#ebebeb').css('border-color', '#cccccc')
  });
 }; 
});

 $(nameParent).find('.add-lift').on('click', function(e){
  e.preventDefault();
  quantity++;
  $(nameParent).find('.quantity-lifts').val(quantity);
  $(nameParent).find('.remove-lift').css('background', '#4c6fd4').css('border-color', '#4c6fd4');
  $(nameParent).find('.remove-lift').hover(function(){
    $(this).css('background', '#4261ba').css('border-color', '#4261ba')
  },
  function(){
    $(this).css('background', '#4c6fd4').css('border-color', '#4c6fd4')
  });
});
};

$('.quantity-lifts').keydown(function(e){
  e.preventDefault();
});


/*   Button for add lift-form  */
var liftForm = $('.calc__hidden');
var numberForm = $('div.calc__form').length;

$('#add_form_lift').on('click', function(e){
  e.preventDefault();
  numberForm++;
  liftForm.clone().addClass('calc__form').addClass('copy'+ numberForm).addClass('index').removeClass('calc__hidden').appendTo('.calc__form-wrap');
  $('.calc__form.copy' + numberForm + ' .num-lift').html(numberForm);
  $('.calc__form.copy' + numberForm + ' .del-lift').addClass('show');

  /*   add atributes "name" for input of copyed forms    */
  $('.calc__form.copy' + numberForm + ' .country').attr('form', 'lifts');
  $('.calc__form.copy' + numberForm + ' .rage-slider-stop').attr('form', 'lifts');
  $('.calc__form.copy' + numberForm + ' .rage-slider-weight').attr('form', 'lifts');
  $('.calc__form.copy' + numberForm + ' .quantity-lifts').attr('form', 'lifts');


  sliderStopsInit('.calc__form.copy' + numberForm);
  sliderWeightInit('.calc__form.copy' + numberForm);
  add_Remove_Lift('.calc__form.copy' + numberForm);
  selectCountyInit('.calc__form.copy' + numberForm);
  delForm('.calc__form.copy' + numberForm);
  hoverDelLift('.calc__form.copy' + numberForm);
}); 


/*   Button for delete form-lift   */
function delForm(nameParent) {
 $(nameParent).find('.del-lift').on('click', function(){
  $(nameParent).remove();
  numberForm = $('div.calc__form').length;
  
  var j = 1;
  $('.index .num-lift').each(function(){
    $(this).html(j);
    j++;

    numberForm = $('div.calc__form').length;
  });  
});
};
/*   Hover effect .del-lift-descr   */
function hoverDelLift(nameParent) {
  $(nameParent).find('.del-lift').hover(function(){
    $(nameParent).find('.del-lift-descr').css('opacity', '1').css('z-index', '1');
  },
  function(){
    $(nameParent).find('.del-lift-descr').css('opacity', '0').css('z-index', '-1');
  });
};


/*   Close modal window   */
$('.modal-close').on('click', function() {
  $('.modal-window').css('transform', 'translate(-75%, -50%)');
  $('.modal-window').css('opacity', '0')
  setTimeout(function(){
    $('.modal-window-wrap').css('opacity', '0');  
  }, 800);
  setTimeout(function(){
    $('.modal-window-wrap').css('z-index', '-1');
  }, 1200);
  scrollDistance = 100;
});


/*   Initializing functions    */
$(document).ready(function(){

  sliderStopsInit('#calc_form');
  sliderWeightInit('#calc_form');
  // selectStructureInit('#calc_form');
  add_Remove_Lift('#calc_form');
  selectCountyInit('#calc_form');
  delForm('#calc_form');

  /*  animations   */
  $('header').addClass('animate');
  hoverDelLift('#calc_form');

  setTimeout(function(){
    $('.container.calc').addClass('animate');
    $('.sitephone').addClass('animate');
  }, 1200);

});


/*   Ajax reguest for mail lifts form   */

$(function(){
  $("#lifts").submit(function(event) {
    event.preventDefault();
    var lifts_data = $('#lifts').serialize();

    $.ajax({
      type: "POST",
      url: "calc.php",
      data: lifts_data,
      success: function() {
       alert("Ваше сообщение отпрвлено!");

       $('.modal-window.lifts').css('transform', 'translate(-75%, -50%)');
       $('.modal-window.lifts').css('opacity', '0')
       setTimeout(function(){
        $('.modal-window-wrap.lifts').css('opacity', '0');  
      }, 800);
       setTimeout(function(){
        $('.modal-window-wrap.lifts').css('z-index', '-1');
      }, 1200);
       scrollDistance = 100;
     },
     error: function() {
      alert("При отправке возникла ошибка");
    }
  });

  });
});    


/*   Submit for lift-form and call cmodal window  */
$('#calculate').on('click', 
  function(){
    $('.modal-window-wrap.lifts').css('z-index', '10').css('opacity', '1');
    setTimeout(function(){
      $('.modal-window.lifts').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
    }, 300);
    scrollDistance = 0;  
  });
