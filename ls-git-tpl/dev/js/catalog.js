$(document).ready(function () {
    catalogAnimate();
});


function catalogAnimate() {
    $('header').addClass('animate');

    setTimeout(function () {
        $('.sitephone').addClass('animate');
        $('.cart-widget').addClass('animate');
    }, 1000);

    setTimeout(function () {
        $('.catalog__cnt h1').addClass('animate');
        $('.catalog__cnt p').addClass('animate');
    }, 1000);

    setTimeout(function () {
        $('.catalog__cnt .row').addClass('animate');
    }, 1300);
};


/* Page category items */
// Select product: 
var markerType = true,
    markerBrand = true;

$(document).click(function (event) {
    if ($(event.target).closest('.select-type-prod').length) return;
    $('.select-type-prod ul').removeClass('active');
    $('.select-type-prod img').removeClass('active');
    event.stopPropagation();
    markerType = true;
});

$(document).click(function (event) {
    if ($(event.target).closest('.select-brand').length) return;
    $('.select-brand ul').removeClass('active');
    $('.select-brand img').removeClass('active');
    event.stopPropagation();
    markerBrand = true;
});

//by type
$('.select-type-prod .select-wrap').click(function () {
    if (markerType == true) {
        $('.select-type-prod ul').addClass('active');
        var typePosition = $('.select-type-prod').offset().top;
        var statusOfcategorySelect = $('.category-items__select').css('position');
        if ($(window).width() > 1023) {
            if (statusOfcategorySelect === 'relative') {
                var dobavit = 100;
                var dobavit1 = 20;
            } else {
                var dobavit = 100;
                var dobavit1 = 20;
            }
        } else {
            if (statusOfcategorySelect === 'relative') {
                var dobavit = 150;
                var dobavit1 = 0;
            } else {
                var dobavit = 150;
                var dobavit1 = 0;
            }
        }

        $('.type_menu_pos').css('top', typePosition - dobavit1);
        //$('.select-type-prod ul').css('top', typePosition - dobavit); //выравнивание выпадающих менюшек из строки поиска
        $('.select-type-prod img').addClass('active');
        markerType = false;
    } else {
        $('.select-type-prod ul').removeClass('active');
        $('.select-type-prod img').removeClass('active');
        markerType = true;
    }
    ;

});

$('.select-type-prod ul li').click(function () {
    $('.select-type-prod ul').removeClass('active');
    $('.select-type-prod img').removeClass('active');
    $('.select-type-prod span').html($(this).html());
    $('.type-product').val($(this).html());
    markerType = true;
    $('.clear-filters').css('color', '#000000');
    $('.clear-filters img').attr('src', 'dev/svg/icons_catalog/Close_red.svg');
});

//by brand
$('.select-brand .select-wrap').click(function () {
    if (markerBrand == true) {
        $('.select-brand ul').addClass('active');
        var proizvoditelPosition = $('.select-brand').offset().top;
        var statusOfcategorySelect = $('.category-items__select').css('position');
        if ($(window).width() > 1023) {
            if (statusOfcategorySelect === 'relative') {
                var dobavit = 100;
                var dobavit1 = 20;
            } else {
                var dobavit = 100;
                var dobavit1 = 20;
            }
        } else {
            if (statusOfcategorySelect === 'relative') {
                var dobavit = 150;
                var dobavit1 = 0;
            } else {
                var dobavit = 150;
                var dobavit1 = 0;
            }
        }
        $('.brand_menu_pos').css('top', proizvoditelPosition - dobavit1);
        //$('.select-brand ul').css('top', proizvoditelPosition - dobavit); //выравнивание выпадающих менюшек из строки поиска
        $('.select-brand img').addClass('active');
        markerBrand = false;
    } else {
        $('.select-brand ul').removeClass('active');
        $('.select-brand img').removeClass('active');
        markerBrand = true;
    }
    ;
});

$('.select-brand ul li').click(function () {
    $('.select-brand ul').removeClass('active');
    $('.select-brand img').removeClass('active');
    $('.select-brand span').html($(this).html());
    $('.brand').val($(this).html());
    markerBrand = true;
    $('.clear-filters').css('color', '#000000');
    $('.clear-filters img').attr('src', 'dev/svg/icons_catalog/Close_red.svg');
});


//Reset filters
$('.clear-filters').on('click', function () {
    $('.select-brand span').html('Производитель');
    $('.select-type-prod span').html('Тип запчасти');
    $('.brand').val('');
    $('.type-product').val('');
    $('.select-type-prod ul').removeClass('active');
    $('.select-type-prod img').removeClass('active');
    $('.select-brand ul').removeClass('active');
    $('.select-brand img').removeClass('active');
    $('.clear-filters').css('color', 'rgb(206, 206, 206)');
    $('.clear-filters img').attr('src', 'dev/svg/icons_catalog/Close_gray.svg');
});


/* Page product */
//Add quantity products:
$('.remove-prod').on('click', function (e) {
    e.preventDefault();
    var input = $(this).parent('.product-quantity').children('.product-quantity__val');
    var quantity = parseInt($(input).val());
    if (quantity != 0) {
        quantity--;
        $(input).val(quantity);
    }
    ;
});

$('.add-prod').on('click', function (e) {
    e.preventDefault();
    var input = $(this).parent('.product-quantity').children('.product-quantity__val');
    var quantity = parseInt($(input).val());
    quantity++;
    $(input).val(quantity);
});


//Add product to cart (styling button)
$('.add-prod__in-cart').on('click', function (e) {
    e.preventDefault();
    $(this).css('background', '#73da41');
    $('.plus-icon').attr('src', 'dev/svg/icons_catalog/Check_white.svg');
    $('.add-prod__in-cart span').html('в корзине');
});


/* Page cart */
//Hover on del-product button:
$('.cart__del-prod').hover(function () {
        $('.cart__del-prod img').attr("src", "dev/svg/icons_catalog/Close_red.svg");
    },
    function () {
        $('.cart__del-prod img').attr("src", "dev/svg/icons_catalog/Close_gray.svg");
    });

//Hover on clearing cart button:
$('.clear-cart').hover(function () {
        $('.clear-cart img').attr("src", "dev/svg/icons_catalog/trash_black.svg");
    },
    function () {
        $('.clear-cart img').attr("src", "dev/svg/icons_catalog/trash_gray.svg");
    });


//Reguest order_form on click
$('.checkout').on('click', function () {
    $('.order-window-wrap').css('z-index', '10').css('opacity', '1');
    setTimeout(function () {
        $('.order-window').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
    }, 300);
    scrollDistance = 0;
});

$('.modal-close').on('click', function () {
    $('.order-window').css('transform', 'translate(-75%, -50%)');
    $('.order-window').css('opacity', '0')
    setTimeout(function () {
        $('.order-window-wrap').css('opacity', '0');
    }, 800);
    setTimeout(function () {
        $('.order-window-wrap').css('z-index', '-1');
    }, 1200);
    scrollDistance = 100;
});
