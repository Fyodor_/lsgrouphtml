
/*   SLIDER   */
$(document).ready(function(){

	/*   slider section-4   */
	setTimeout(function(){
		$('.equipment__slider').slick({
			dots: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 5000,
			infinite: true,
			fade: true,
			pauseOnHover: false,
			// responsive: [
			// {
			// 	breakpoint: 1024,
			// 	settings: {
			// 		autoplay: false
			// 	}
			// }
			// ]
		});
		$('.equipment__slider').find('.slick-dots').addClass('dots-equipment');
	}, 3000);

	/*   slider section-5   */
	$('.section-5__slider').slick({
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3000,
		infinite: true,
		fade: true,
		// responsive: [
		// {
		// 	breakpoint: 1024,
		// 	settings: {
		// 		autoplay: false
		// 	}
		// }
		// ]
	});

	/*   add description for slides*/
	$('.slide-active-descr').html('Бизнес-центры');

	$('.section-5__slider').on('afterChange', function(event, slick, currentSlide){
		var numberSlide = currentSlide;
		switch (numberSlide) {
			case 0: $('.slide-active-descr').html('Бизнес-центры'); break;
			case 1: $('.slide-active-descr').html('Торговые центры'); break;
			case 2: $('.slide-active-descr').html('Жилищные комплексы'); break;
		}
	});

	/*   slider section-6   */
	$('.section-6__slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 3,
		infinite: false,
		// centerMode: true
		responsive: [
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2,
			}

		},
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});

});


/*   INTERVALS  section-7   */
var counterLifts = 0;
counterEquip = 0;
counterService = 0;
counterEmployees = 0;

marker = true;

/*   lifts   */	
function goLifts(){	
	var timerLifts = setInterval(function() {			
		$('#lifts').html(counterLifts + '+');
		if(counterLifts >= 1000 ) {
			clearInterval(timerLifts);
		};
		counterLifts+=4;
	}, 1);
	marker = false;
}

/*   lift equipment   */
function goStaff(){
	var timerEquipment = setInterval(function() {			
		$('#staff').html(counterEquip + '%');
		if(counterEquip >= 100) {
			clearInterval(timerEquipment);
		};
		counterEquip++;
	}, 20 );
	marker = false;
}

/*   lift services   */
function goTransport(){
	var timerService = setInterval(function() {			
		$('#transport').html(counterService + '+');
		if(counterService >= 40) {
			clearInterval(timerService);
		};
		counterService++;
	}, 50);
	marker = false;
}

/*   employees   */
function goEmployees(){
	var timerEmployees = setInterval(function() {	

		$('#employees').html(counterEmployees + '+');
		if(counterEmployees >= 100) {
			clearInterval(timerEmployees);
		};
		counterEmployees++;
	}, 20);
	marker = false;
};

$(window).on('scroll', (function(){
	var distanse_counter = $('.section-6').offset().top;

	if($(window).scrollTop() > distanse_counter) {		
		if(marker) {
			goLifts();
			goStaff();
			goTransport();
			goEmployees();		
		}	
	}	
}));


/*   HOVER EFFECTS section-8   */
$('.more-img').hover(function(){
	$(this).css('background', '#3d6ccb');
	$('.more-img img').attr("src","dev/svg/zoom_w.svg");
}, function(){
	$(this).css('background', '#fff');
	$('.more-img img').attr("src","dev/svg/zoom_b.svg");
});

$('.order-img').hover(function(){
	$(this).css('background', '#3d6ccb');
	$('.order-img img').attr("src","dev/svg/bag_w.svg");
}, function(){
	$(this).css('background', '#fff');
	$('.order-img img').attr("src","dev/svg/bag_b.svg");
});


/*   SMOOTH SCROLL UP   */
$('.footer-logo').on('click', 'a', function(even){
	even.preventDefault();
	var link = $(this).attr('href');
	var distanse = $(link).offset().top;
	$('html, body').animate({
		scrollTop: distanse
	}, 1000);
});

$('.header-logo').on('click', 'a', function(even){
	even.preventDefault();
	var link = $(this).attr('href');
	var distanse = $(link).offset().top;
	$('html, body').animate({
		scrollTop: distanse
	}, 1000);
});


/*   MODAL WINDOW on click  section-8__link-prop  */
$('.section-8__link-prop').on('click', function() {
	$('.modal-window-wrap').css('z-index', '10').css('opacity', '1');
	setTimeout(function(){
		$('.modal-window').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
	}, 300);
	scrollDistance = 0;	
});

/*   ajax reguest for mail message section-9 form   */
$(function(){
    $("#call_form").submit(function(event) { //устанавливаем событие отправки для формы с id=form
    	event.preventDefault();
            var form_data = $(this).serialize(); //собераем все данные из формы
            $.ajax({
            type: "POST", //Метод отправки
            url: "call.php", //путь до php фаила отправителя
            data: form_data,
            success: function() {
                   //код в этом блоке выполняется при успешной отправке сообщения
                   alert("Ваше сообщение отпрвлено!");
                   $('#call_form input').val('').prop('checked', false);
               },
               error: function() {
               	alert("При отправке возникла ошибка");
               }
           });

        });
});    


/*   animations homepage  */
$(document).ready(function(){
	// if($(window).width() > 1024) {
		homeLoadAnimate();
		homeScrollAnimate();
		animateSectionHome();
	// };
});

// $(window).resize(function(){
// 	if($(window).width() > 1024) {
// 		homeLoadAnimate();
// 		homeScrollAnimate();
// 	};
// });


function homeLoadAnimate() {
	$('header').addClass('animate');
	$('.section-1').addClass('animate');

	setTimeout(function(){
		$('.banner').addClass('animate');
	}, 800);

	setTimeout(function(){
		$('.section-1__title').addClass('animate');
		$('.section-1__title-descr').addClass('animate');
	}, 1200);

	setTimeout(function(){
		$('.link.ion-ios-arrow-right').addClass('animate');
		$('.sitephone').addClass('animate');
		$('.section-2').addClass('animate');
	}, 1500);
};

function homeScrollAnimate() {
	$(window).scroll(function(){
		animateSectionHome();
	});
};

function animateSectionHome(){
	var distanseSection3 = 300,
	distanseSection4 = $('.section-3').offset().top,
	distanseSection5 = $('.section-4').offset().top,
	distanseSection7 = $('.section-6').offset().top,
	distanseSection8 = $('.section-7').offset().top,
	distanseSection9 = $('.section-8').offset().top;

	if ($(window).scrollTop() > distanseSection3){
		$('.section-3').addClass('animate');
	};

	if ($(window).scrollTop() > distanseSection4){
		$('.section-4').addClass('animate');
	};

	if ($(window).scrollTop() > distanseSection7){
		$('.section-7').addClass('animate');
	};

	if ($(window).scrollTop() > distanseSection8 + 200){
		$('.section-8__cnt').addClass('animate');
	};

	if ($(window).scrollTop() > distanseSection9 + 600){
		$('.section-9').addClass('animate');
	};
};