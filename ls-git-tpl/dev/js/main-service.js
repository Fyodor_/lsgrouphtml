
/*   Modal window on click  section-8__link-prop  */
$('.button-prop').on('click', function() {
	$('.modal-window-wrap').css('z-index', '10').css('opacity', '1');
	setTimeout(function(){
		$('.modal-window').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
	}, 300);
	scrollDistance = 0;	
});


/*   Slider   */
$(document).ready(function(){
	/*   equipment slider   */
	$('.equipment__slider').slick({
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		fade: true,
		pauseOnHover: false
	});
	$('.equipment__slider').find('.slick-dots').addClass('dots-equipment');

	/*   Animations main-service on load   */
	// if($(window).width() > 1199) {
		mainServLoadAnimate();
		mainServScrollAnimate();
	// };
});

/*   Animations main-service on resize  */
// $(window).resize(function() {
// 	if($(window).width() > 1199) {
// 		mainServLoadAnimate();
// 		mainServScrollAnimate();
// 		animateSectionMainServ();
// 	};
// });


function mainServLoadAnimate(){
	$('header').addClass('animate');
	$('.main-service__section-1').addClass('animate');

	setTimeout(function(){
		$('.banner.main').addClass('animate');
	}, 1000);

	setTimeout(function(){
		$('.services__section-1-title').addClass('animate');
	}, 1300);

	setTimeout(function(){
		$('.sitephone').addClass('animate');
		$('.main-service__section-2-descr').addClass('animate');
		$('.main-service__section-2-cnt').addClass('animate');	
	}, 1600);
};

function mainServScrollAnimate() {
	$(window).scroll(function(){
		animateSectionMainServ();
	});
};

function animateSectionMainServ() {
	var mainServiceSection4 = $('.main-service__section-4').offset().top;
	if ($(window).scrollTop() > mainServiceSection4 - 700){
		$('.main-service__section-4 .container').addClass('animate');
	};
	if ($(window).scrollTop() > mainServiceSection4 - 400){
		$('.main-service__section-5').addClass('animate');
	};
};
