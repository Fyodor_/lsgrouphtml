/*__________/JQUERY PLUGINS\__________*/

/*   Smooth scroll   */
var scrollIndex = 1;

if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i)) {
    scrollIndex = 0;
}
;

var scrollTime = 0.3;	   //Scroll time
var scrollDistance = 100; //Distance. Use smaller value for shorter scroll and greater value for longer scroll

$(function () {
    if (scrollIndex == 1) {
        var $window = $(window); //Window object

        $window.on("mousewheel DOMMouseScroll", function (event) {
            event.preventDefault();
            var delta = event.originalEvent.wheelDelta / 120 || -event.originalEvent.detail / 3;
            var scrollTop = $window.scrollTop();
            var finalScroll = scrollTop - parseInt(delta * scrollDistance);

            TweenMax.to($window, scrollTime, {
                scrollTo: {y: finalScroll, autoKill: true},
                ease: Power1.easeOut,	//For more easing functions see https://api.greensock.com/js/com/greensock/easing/package-detail.html
                autoKill: true,
                overwrite: 5
            });
        });
    }
    ;
});


/*   Input-mask  */
var maskTel = $(function maskInput() {
    $('.form-phone').mask("+7(999)999-99-99");
});

/*___________________________________*/


/*  Smooth transition beetween pages   */
$(document).ready(function () {
    // $("body").css("display", "none");
    // $("body").fadeIn(500);
    $(".transition").click(function (event) {
        event.preventDefault();
        linkLocation = this.href;
        $("body").fadeOut(500, redirectPage);
    });

    function redirectPage() {
        window.location = linkLocation;
    };
});


/*   Modal window (.sitephone)   */
$('.sitephone').on('click', function () {
    $('.modal-window-wrap').css('z-index', '10').css('opacity', '1');
    setTimeout(function () {
        $('.modal-window').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
    }, 300);
    scrollDistance = 0;
});

$('.sitephone__media-768').on('click', function () {
    $('.modal-window-wrap').css('z-index', '10').css('opacity', '1');
    setTimeout(function () {
        $('.modal-window').css('opacity', '1').css('transform', 'translate(-50%, -50%)');
    }, 300);
    scrollDistance = 0;
});


$('.modal-close').on('click', function () {
    $('.modal-window').css('transform', 'translate(-75%, -50%)');
    $('.modal-window').css('opacity', '0')
    setTimeout(function () {
        $('.modal-window-wrap').css('opacity', '0');
    }, 800);
    setTimeout(function () {
        $('.modal-window-wrap').css('z-index', '-1');
    }, 1200);
    scrollDistance = 100;
});


/*   Hover effect (.sitephone)   */
$('.sitephone').hover(function () {
        $(this).css('box-shadow', '0 0 0 rgba(49,107,221, 0.35)');
        $('.sitephone-descr').css('opacity', '1').css('width', '200px').css('font-size', '15px').css('right', '50px');
    },
    function () {
        $(this).css('box-shadow', '0 10px 20px rgba(49,107,221, 0.35)');
        $('.sitephone-descr').css('opacity', '0').css('width', '0').css('font-size', '0').css('right', '60px');
        // $('.sitephone-descr').css('z-index', '-1').css('opacity', '0').css('transform', 'scaleX(0)');
    });


/*   Validation forms   */
$('.form-name').focusout(function () {
    var valueName = $(this).val(),
        regularName = /^[А-Я][а-яА-ЯёЁ]{2,20}/;
    if (!regularName.test(valueName)) {
        $(this).css('border-color', '#ff7373');
    } else {
        $(this).css('border-color', '#7b96cd');
    }
    ;
});

$('.form-phone').focusout(function () {
    var valuePhone = $(this).val(),
        regularPhone = /^\+?[78][-\(]?\d{3}\)?-?\d{3}-?\d{2}-?\d{2}$/;
    if (!regularPhone.test(valuePhone)) {
        $(this).css('border-color', '#ff7373');
    } else {
        $(this).css('border-color', '#7b96cd');
    }
    ;
});

$('.form-mail').focusout(function () {
    var valuePhone = $(this).val(),
        regularPhone = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/;
    if (!regularPhone.test(valuePhone)) {
        $(this).css('border-color', '#ff7373');
    } else {
        $(this).css('border-color', '#7b96cd');
    }
    ;
});


/*   Hover effects footer   */
$('.company-group img').hover(function () {
    $(this).attr("src", "dev/svg/liftstroy_logo_w.svg");
}, function () {
    $(this).attr("src", "dev/svg/Liftstroy_footer.svg");
});


/*   Ajax reguest for mail message sitephone form   */
var namePage = $('title').text();
$('#page_name').val(namePage);

$(function () {
    $("#reguest_form").submit(function (event) {
        event.preventDefault();
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "call.php",
            data: form_data,
            success: function () {
                alert("Ваше сообщение отправлено!");
                $('#reguest_form input').val('').not(':hidden').prop('checked', false);

                $('.modal-window').css('transform', 'translate(-75%, -50%)');
                $('.modal-window').css('opacity', '0')
                setTimeout(function () {
                    $('.modal-window-wrap').css('opacity', '0');
                }, 800);
                setTimeout(function () {
                    $('.modal-window-wrap').css('z-index', '-1');
                }, 1200);
                scrollDistance = 100;
            },
            error: function () {
                alert("При отправке возникла ошибка");
            }
        });
    });
});

/*====================search menu stop at top - 2nd variant (custom)======================*/
var scrollPos = 0;
var stickPositionStop = 50;
var stickPosition = 100;
$(window).scroll(function () {
    var st = $(this).scrollTop();
    if (st < scrollPos) {
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        //console.log('Текущая прокрутка сверху: ' + scrollTop);
        if (scrollTop < stickPositionStop) {
            $('#stick').css('position', '');
            $('#stick').css('top', '');
            $('#stick').css('width', '');
            $('.type').css('top', '');
        }
        //пересчет размера выпадающего меню с результатами поиска при фиксировании окна поиска
        var forRightSize = 32;
        var searchWidth = $('#search').width();
        $('.search_result_window').css('width', searchWidth - forRightSize);
    } else {
        //var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        //console.log('Текущая прокрутка сверху: ' + scrollTop);
        var stick = document.getElementById('stick');
        if (stick.getBoundingClientRect().top < stickPosition) {
            $('#stick').css('position', 'fixed');
            $('#stick').css('width', '100%');
            if ($(window).width() > 576) {
                if (!$('header').hasClass('open')) {
                    $('#stick').css('top', '70px');
                } else {
                    $('#stick').css('top', '100px');
                }
            } else {
                $('#stick').css('top', '70px');
            }
        }
        //пересчет размера выпадающего меню с результатами поиска при фиксировании окна поиска
        var forRightSize = 32;
        var searchWidth = $('#search').width();
        $('.search_result_window').css('width', searchWidth - forRightSize);
    }
    scrollPos = st;
});

/* animate on search*/
$(document).ready(function () {
    setTimeout(function () {
        $('.search_resalt_text').addClass('animate');
    }, 1000);
});

/*send to the backend from search*/
$(document).ready(function () {
// Сюда указываем id поля поиска.
    $('body').on('keyup', '#search', searchFromBase);
    $('body').on('click', '.search_icon', takeFromBase);
    $('body').on('click', '.summ_search_result', takeFromBase);

    function searchFromBase(e) {
        // В переменную помещаем поисковое значение которое ввел пользователь.
        var input_search = $("#search").val();
        var forRightSize = 32;
        // Проверяем поисковое значение. Если оно больше или ровняется Трём, то всё нормально и также если меньше 150 символов.
        if (input_search.length >= 3 && input_search.length < 150) {
            // Делаем запрос в обработчик в котором будет происходить поиск.
            if (e.which === 13) {
                takeFromBase();
                $('#search_result_window').removeClass('search_result_window');
                $('#search_result_window').addClass('search_result_window_hidden');
            } else {
                $.ajax({
                    type: "POST",
                    url: "search.php", // Обработчик.
                    data: "q=" + input_search, // В переменной <strong>q</strong> отправляем ключевое слово в обработчик.
                    dataType: "html",
                    cache: false,
                    success: function (data) {
                        $('#search_result_window').html(data);// Добавляем в список результат поиска.
                        $('#search_result_window').removeClass('search_result_window_hidden');
                        $('#search_result_window').addClass('search_result_window');// Показываем блок с результатом.
                    },
                    error: function () {

                        $('#search_result_window').removeClass('search_result_window_hidden');
                        $('#search_result_window').addClass('search_result_window');
                        var searchWidth = $('#search').width();
                        $('.search_result_window').css('width', searchWidth - forRightSize);
                    }
                });
            }

        } else {
            $('#search_result_window').removeClass('search_result_window');
            $('#search_result_window').addClass('search_result_window_hidden');
        }
    };
    /*close if click outside*/
    $(document).mouseup(function (e) {
        if ($('#search_result_window').hasClass('search_result_window')) {
            var ids = e.target.id;
            if (ids !== 'search') {
                $('#search_result_window').removeClass('search_result_window');
                $('#search_result_window').addClass('search_result_window_hidden');
            }
        }
    });

    function takeFromBase() {
        // В переменную помещаем поисковое значение которое ввел пользователь.
        var input_search = $("#search").val();
        // Проверяем поисковое значение. Если оно больше или ровняется Трём, то всё нормально и также если меньше 150 символов.
        if (input_search.length >= 3 && input_search.length < 150) {
            // Делаем запрос в обработчик в котором будет происходить поиск.
            $.ajax({
                type: "POST",
                url: "search.php", // Обработчик.
                data: "t=" + input_search, // В переменной <strong>q</strong> отправляем ключевое слово в обработчик.
                dataType: "html",
                cache: false,
                success: function (data) {
                    $('.category-items__card-cnt').html(data);// Добавляем в список результат поиска.
                    //$('#search_result_window').removeClass('search_result_window_hidden');
                    //$('#search_result_window').addClass('search_result_window');// Показываем блок с результатом.
                },
                error: function () {
                    //$('#search_result_window').removeClass('search_result_window_hidden');
                    //$('#search_result_window').addClass('search_result_window');
                    $('.category-items__card-cnt').html(input_search);
                }
            });
        } else {
            //$('#search_result_window').removeClass('search_result_window');
            //$('#search_result_window').addClass('search_result_window_hidden');
        }
    }

    /*small_filter_button*/
    $('body').on('click', '.small_filter_button', filterSmallMenu);

    function filterSmallMenu() {
        $('.filters').fadeIn(300);
    }

    $('body').on('click', '#close_big_filters', filtersClose);

    function filtersClose() {
        $('.filters').fadeOut(300);
    }

    /*settings_button (small menu)*/
    $('body').on('click', '.settings_button', settingsButton);

    function settingsButton() {
        var stick = document.getElementById('stick');
        $('#stick').css('position', 'fixed');
        $('#stick').css('width', '100%');
        if ($(window).width() > 576) {
            if (!$('header').hasClass('open')) {
                $('#stick').css('top', '70px');
            } else {
                $('#stick').css('top', '100px');
            }
        } else {
            $('#stick').css('top', '70px');
        }


        $('#stick').toggleClass('category-items__select_with_button');
        $('.settings_button_small_menu').slideToggle(200);
        $('#small_menu').toggleClass('showSetImg');
        $('#accordeon .acc-body').not($(this).next()).slideUp(200);
    }

    //прикрепляем клик по заголовкам acc-head
    $('#accordeon .acc-head').on('click', f_acc);

    function f_acc() {
//скрываем все кроме того, что должны открыть
        $('#accordeon .acc-body').not($(this).next()).slideUp(200);
// открываем или скрываем блок под заголовоком, по которому кликнули
        $(this).next().slideToggle(200);
    }

    $('body').on('click', '.acc-body li', function () {
        $(this).addClass('activ_small_menu');
        $('.clear-filters-small img').attr('src', 'dev/svg/icons_catalog/Close_red.svg');
        $('.clear-filters-small').css('color', '#000000');
        $('#accordeon').slideUp(200);
        $('#stick').toggleClass('category-items__select_with_button');
    });

    $('body').on('click', '.clear-filters-small', function () {
        $('.acc-body li').removeClass('activ_small_menu');
        $('.clear-filters-small img').attr('src', 'dev/svg/icons_catalog/Close_gray.svg');
        $('.clear-filters-small').css('color', '#a6a6a6');
        $('#accordeon').slideUp(200);
        $('#stick').toggleClass('category-items__select_with_button');
    });
    /*==========Изменение положения синей кнопки на маленьком экране при фокусе на поиске(клавиатура появится)===*/
    $('#search').focus(function () {
        $('.small_filter_button').css('bottom', '-100%');
    });
    $('#search').blur(function () {
        $('.small_filter_button').css('bottom', '20%');
    });

});




