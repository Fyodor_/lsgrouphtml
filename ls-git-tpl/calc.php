<?
if(
    (isset($_POST['country'])&&$_POST['country']!="")&&
    (isset($_POST['stops'])&&$_POST['stops']!="")&&
    (isset($_POST['weight'])&&$_POST['weight']!="")&&
    (isset($_POST['quantity'])&&$_POST['quantity']!="")&&

    (isset($_POST['lifts-form-name'])&&$_POST['lifts-form-name']!="")&&
    (isset($_POST['lifts-form-mail'])&&$_POST['lifts-form-mail']!="")&&
    (isset($_POST['lifts-form-tel'])&&$_POST['lifts-form-tel']!="")
    ){

        $carrying = array();
        for ($i=0; $i < count($_POST['weight']); $i++) {  
            switch ($_POST['weight'][$i]) {
                case '1':
                $carrying[] = 'Менее 400кг';
                break;
                case '2':
                $carrying[] = 'От 400кг до 1000кг';
                break;
                case '3':
                $carrying[] = 'Более 1000кг';
                break;
            };
        };

        $liftData;
        for ($i=0; $i < count($_POST['country']); $i++) {
            $numLift = $i + 1;
            $liftData.= 
            '<p>Лифт-'.$numLift.'</p>
            <p>Производитель лифта: '.$_POST['country'][$i].'</p>
            <p>Количество остановок: '.$_POST['stops'][$i].'</p>
            <p>Грузоподъемность: '.$carrying[$i].'</p>
            <p>Количество лифтов с заданными параметрами: '.$_POST['quantity'][$i].'</p><hr>';
        };

        $message = '
        <html>
        <head>
            <title>'.$subject.'</title>
        </head>
        <body>
            <p>Имя: '.$_POST['lifts-form-name'].'</p>
            <p>Телефон: '.$_POST['lifts-form-tel'].'</p> 
            <p>E-mail: '.$_POST['lifts-form-mail'].'</p>
            <hr>'.$liftData.'
        </body>
        </html>';

        $to = 'info@lsgl.ru'; 
        $subject = 'Заявка. Страница "Калькулятор"';
        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: Отправитель <from@example.com>\r\n";
        mail($to, $subject, $message, $headers);
    }
?>